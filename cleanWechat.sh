#! /bin/bash
folder=~/Library/Containers/com.tencent.xinWeChat/Data/Library/Application\ Support/com.tencent.xinWeChat/2.0b4.0.9
files=$(ls -a "$folder")
for file in $files
do
    if [ ${#file} -eq 32 ]
    then
        rm -rf "${folder}/$file"
    fi
done
